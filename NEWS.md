# ubatheme (development version)

# ubatheme 0.0.0.9000

* Created the package
* Added the `make_uba_plot()` function
* Added the `background()` function
* Added the `uba_theme()` function
* Added an external vignette
