#' Colour values in accordance with UBA-CD
#'
#' @description `r badge("experimental")` Colour values in accordance with UBA-CD.
#'
#' @param colour colour names. Insert as string: grün, blau, flieder, fuchsia, ocker, grau
#' @param brightness colour brightness. Insert as string: hell, dunkel
#'
#' @return RGB-Codes
#' @export
#'
#' @examples
#' # complete palette as dataframe
#' pal <- uba_pal()
#'
#' cols <- pivot_longer(pal, hell:dunkel) %>%
#'    filter(farbe != "grau") %>%
#'    pull(value)
#'
#' # select only hex-values for dark colours
#' uba_pal()[["dunkel"]]
#'
#' # select only hex-value for bright green
#' uba_pal("grün", "hell")

uba_pal <- function(colour, brightness) {
  rgb_to_hex <- function(x) rgb(x[1], x[2], x[3], maxColorValue = 255)

  cols_uba <- c("grün", "blau", "flieder", "fuchsia", "ocker", "grau")

  pal <- data.frame(
    farbe = cols_uba,
    hell = unlist(lapply(list(c(94, 173, 53), c(0, 155, 213), c(157, 87, 154), c(206, 31, 94), c(250, 187, 0), c(240, 241, 241)), rgb_to_hex)),
    dunkel = unlist(lapply(list(c(0, 118, 38), c(0, 95, 133), c(98, 47, 99), c(131, 5, 60), c(215, 132, 0), c(75, 75, 77)), rgb_to_hex))
  )


  if (missing(colour) & missing(brightness)) {
    return(pal)
  } else {
    if (!colour %in% c("grün", "blau", "flieder", "fuchsia", "ocker", "grau")) stop(paste("object", colour, "not found. Options are", paste(cols_uba, collapse = ", ")))
    if (!brightness %in% c("hell", "dunkel")) stop(paste("object", brightness, "not found. Options are", paste(c("hell", "dunkel"), collapse = ", ")))

    col_tmp <- pal[pal[["farbe"]] == colour,]
    col_tmp <- col_tmp[[brightness]]

    return(col_tmp)
  }
}
