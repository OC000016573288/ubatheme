test_that("check linecharts", {
  source(test_path("helper.R"), local = FALSE, encoding = "UTF-8")

  p_tmp <- df %>%
    ggplot(aes(Zeit, Werte, colour = Daten)) +
    geom_line(linewidth = 2.25/.pt) + # identity/stack
    geom_point(size = 5/.pt) + # identity/stack
    scale_colour_manual(values = cols) +
    scale_x_continuous(breaks = br, minor_breaks = br_minor, labels = br, limits = range(br), expand = expansion(0)) +
    scale_y_continuous(expand = expansion(add = c(0, 5))) +
    coord_cartesian(clip = "off") +
    labs

  p <- make_uba_plot(p_tmp, grid_type = "xmajor_ymajor", ticks_type = "x_axis")
  ggsave(test_path("diagrams","08_Beispiel_Linie.png"), p, height = 105, width = 148, units = "mm", dpi = 300, bg = "white")

  p_tmp <- df %>%
    filter(Daten %in% c("Daten1", "Daten10")) %>%
    ggplot(aes(Zeit, Werte, colour = Daten)) +
    geom_point(size = 5/.pt) +
    scale_colour_manual(values = cols[c(1, 10)]) +
    scale_x_continuous(breaks = br, minor_breaks = br_minor, labels = br, limits = range(br), expand = expansion(0)) +
    scale_y_continuous(expand = expansion(add = c(0, 5))) +
    coord_cartesian(clip = "off") +
    labs

  p <- make_uba_plot(p_tmp, grid_type = "xmajor_ymajor", ticks_type = "x_axis")
  ggsave(test_path("diagrams","09_Beispiel_Punkt.png"), p, height = 105, width = 148, units = "mm", dpi = 300, bg = "white")


  # Punkt-Liniendiagramm gestapelt
  p_tmp <- df %>%
    ggplot(aes(Zeit, Werte, colour = Daten)) +
    geom_line(position = "stack", linewidth = 2.25/.pt) + # identity/stack
    geom_point(position = "stack", size = 5/.pt) + # identity/stack
    scale_colour_manual(values = cols) +
    scale_x_continuous(breaks = br, minor_breaks = br_minor, labels = br, limits = range(br)) +
    scale_y_continuous(expand = expansion(add = c(0, 5))) +
    coord_cartesian(clip = "off") #+
  # labs

  p <- make_uba_plot(p_tmp, grid_type = "xmajor_ymajor", ticks_type = "x_axis")
  ggsave(test_path("diagrams","09_Beispiel_Linie_gestapelt.png"), p, height = 105, width = 148, units = "mm", dpi = 300, bg = "white")

  # line with regression and confidenceinterval
  p_tmp <- df |>
    filter(Daten %in% c("Daten1", "Daten2")) |>
    ggplot(aes(Zeit, Werte, colour = Daten, fill = Daten)) +
    geom_smooth(linewidth = 2.25/.pt) +
    scale_colour_manual(values = uba_pal()[["dunkel"]]) +
    scale_fill_manual(values = uba_pal()[["hell"]]) +
    scale_x_continuous(breaks = br, minor_breaks = br_minor, labels = br, limits = range(br), expand = expansion(0)) +
    scale_y_continuous(limits = c(NA, NA), expand = expansion(0)) +
    coord_cartesian(clip = "off") +
    labs

  p <- make_uba_plot(p_tmp, grid_type = "xmajor_ymajor", ticks_type = "x_axis")
  ggsave(test_path("diagrams","13_Beispiel_Linie_Konfi.png"), p, height = 105, width = 148, units = "mm", dpi = 300, bg = "white")


  showtext_auto(enable = FALSE)
})
