### Karten

Ebenso können Karten nach demselben prinzip erstellt werden, wobei die Füllfarbe auf den UBA-Farben basieren sollte. Die Bundeslandumrisse sind vereinfachte Verwaltungsgebiete (© GeoBasis-DE / BKG (2021)). Es werden 16 Farben benötigt, wobei das UBA-CD nur 12 vorgibt. Deshalb wird basierend auf vier Grundfarben ein Farbverlauf gebildet, der optisch ansprechend ist.

```{r}
de_border <- read_sf(here::here("tests", "testthat", "helper_geom", "2023-10-02_DE_Grenze.geojson"))

map_labs <- labs(title = "Haupttitel",
                          subtitle = "Untertitel",
                          caption = c("*Fußnote", "Quelle: Quellenangabe"),
                          x = "",
                          y = "")

cols_start <- c(
  uba_pal("fuchsia", "dunkel"),
  uba_pal("blau", "dunkel"),
  uba_pal("grün", "dunkel"),
  uba_pal("ocker", "hell")
)

cols_ramp <- colour_ramp(cols_start, na.color = uba_pal("grau", "hell"), alpha = FALSE)
cols_tmp <- cols_ramp(seq(0, 1, length = 16))

p_tmp <- ggplot(de_border) +
  geom_sf(aes(fill = Bundesland)) +
  coord_sf(label_axes = "----", clip = "off") +
  map_labs +
  scale_fill_manual(values = cols_tmp) +
  guides(fill = guide_legend(ncol = 2))

p <- make_uba_plot(p_tmp, hatch = "panel")
ggsave(here::here("tests", "testthat", "diagrams", "12_Beispiel_Karte_Hochkant.png"), p, height = 148, width = 105, units = "mm", dpi = 300, bg = "white")

p_tmp <- ggplot(de_border) +
  geom_sf(aes(fill = Bundesland)) +
  coord_sf(label_axes = "----", clip = "off") +
  map_labs +
  scale_fill_manual(values = cols_tmp) +
  guides(fill = guide_legend(ncol = 4))

p <- make_uba_plot(p_tmp, hatch = "panel")
ggsave(here::here("tests", "testthat", "diagrams", "12_Beispiel_Karte_Querformat.png"), p, height = 2*105, width = 2*148, units = "mm", dpi = 300, bg = "white")
```

```{r map-portrait, echo=FALSE, warning=FALSE, message=FALSE, fig.align = 'center', out.width = "100%", fig.cap = "Beispielkarte im Hochformat.", fig.alt="Beispielkarte im Querformat."}
knitr::include_graphics(here::here("tests", "testthat", "diagrams", "12_Beispiel_Karte_Hochkant.png"))
```

```{r map-landscape, echo=FALSE, warning=FALSE, message=FALSE, fig.align = 'center', out.width = "100%", fig.cap = "Beispielkarte im Querformat und DIN A4.", fig.alt="Beispielkarte im Querformat und DIN A4."}
knitr::include_graphics(here::here("tests", "testthat", "diagrams", "12_Beispiel_Karte_Querformat.png"))
```
